import { createRouter, createWebHistory } from 'vue-router'
import theToDo from '../components/TheToDoList.vue'
import theWeather from '../components/TheWeather.vue'
import theExchanger from '../components/TheExchanger.vue'

const routes = [
    {
        path: '/',
        name: 'ToDo',
        component: theToDo,
    },
    {
        path: '/weather',
        name: 'Weather',
        component: theWeather,
    },
    {
        path: '/currencies',
        name: 'Exchanger',
        component: theExchanger,
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router
